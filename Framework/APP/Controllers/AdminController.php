<?php
require 'app/Models/Conexion.php';
require 'app/Models/Admin.php';
use UPT\conexion;
use UPT\Admin;

class AdminController
{
    public function __construct()
    {
        if($_GET["action"]== "candado"){
            if(!isset($_SESSION["usuario"])){
                echo "No has iniciado sesion";

                header("Location:Index.php?controller=usuario&action=loaginAdmin");
            }
        }
    }
    public function credencialesAdmin(){

        $correo = $_POST["correo"];
        $password = $_POST["password"];
        $status="Sesion iniciada";
        $verificar= Admin::verificarAdmin($correo,$password);
        if(!$verificar){
            $status="Datos Incorrectos";
        }else{

            $_SESSION["Admin"]=$verificar;
            require "app/Views/Administrador.html";
        }
    }
    public function lougt(){

        if (isset($_SESSION["Usuario"])){
            unset($_SESSION["Usuario"]);

        }
        require "app/Views/Principal.html";
    }
    function candado(){

        require "app/Views/Administrador";
    }
    function verificaAdmin(){
        $admin = new Admin();
        $admin->nombre = $_POST["nombre"];
        $admin->apellidoPaterno = $_POST["apellidoPaterno"];
        $admin->apellidoMaterno = $_POST["apellidoMaterno"];
        $admin->puesto = $_POST["puesto"];
        $admin->correo = $_POST["correo"];
        $admin->password = $_POST["password"];
        $admin->crear();

        require "app/Views/loaginAdmin.html";
    }

    function loginAdmin(){
        require "app/Views/loaginAdmin.html";
    }
    function agregarAd(){
        require "app/Views/agregarAdmin.html";
    }
    function Admin(){
        require "app/Views/Administrador.html";
    }

}