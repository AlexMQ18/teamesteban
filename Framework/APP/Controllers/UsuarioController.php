<?php
require 'app/Models/Conexion.php';
require 'app/Models/Usuario.php';
use UPT\conexion;
use UPT\Usuario;

class UsuarioController
{
    public function __construct()
    {
        if($_GET["action"]== "candado"){
            if(!isset($_SESSION["usuario"])){
                echo "No has iniciado sesion";

                header("Location:Index.php?controller=usuario&action=vistaLogin");
            }
        }
    }


    public function verificarCredenciales(){

        $correo = $_POST["correo"];
        $password = $_POST["password"];
        $status="Sesion iniciada";
        $verificar= Usuario::verificarUsuario($correo,$password);
        if(!$verificar){
            $status="Datos Incorrectos";
        }else{

            $_SESSION["Usuario"]=$verificar;
            require "app/Views/PrincipalS.html";
        }
    }
    public function lougt(){

        if (isset($_SESSION["Usuario"])){
        unset($_SESSION["Usuario"]);

        }
        require "app/Views/Principal.html";
    }
    function candado(){

        require "app/Views/PrincipalS.html";
    }


    function verificaRegistro(){
        $usuario = new Usuario();
        $usuario->nombre = $_POST["nombre"];
        $usuario->apellidoPaterno = $_POST["apellidoPaterno"];
        $usuario->apellidoMaterno = $_POST["apellidoMaterno"];
        $usuario->correo = $_POST["correo"];
        $usuario->fechaNaci = $_POST["fechaNaci"];
        $usuario->password = $_POST["password"];
        $usuario->crear();

        require "app/Views/login.html";
    }


    //VISTAS PAGINAS
    function principal(){
        require "app/Views/Principal.html";
    }


    function vistaLogin(){
        require "app/Views/login.html";
    }

    function registro(){
        require "app/Views/Registros.html";
    }
    function futbol(){
        require "app/Views/Futbol.html";
    }
    function beisb(){
        require "app/Views/Beisb.html";
    }
    function basquetbol(){
        require "app/Views/Basquetbol.html";
    }
    function tenis(){
        require "app/Views/Tenis.html";
    }
    function box(){
        require "app/Views/Box.html";
    }
    function compra(){
    require "app/Views/Compra.html";
}
    function descuento(){
        require "app/Views/Descuentos.html";
    }
    function futbolS(){
        require "app/Views/FutbolS.html";
    }
    function beisbS(){
        require "app/Views/BeisbS.html";
    }
    function basquetbolS(){
        require "app/Views/BasquetbolS.html";
    }
    function tenisS(){
        require "app/Views/TenisS.html";
    }
    function boxS(){
        require "app/Views/BoxS.html";
    }
    function compraS(){
        require "app/Views/CompraS.html";
    }
    function descuentoS(){
        require "app/Views/DescuentosS.html";
    }
    function principalS(){
        require "app/Views/Principal.html";
    }

    function todo(){
        $todos=Usuario::all();
        echo json_encode($todos);

}
    function tablaA(){
        require "app/Views/TablaAccesorios.html";
    }

}