<?php


namespace UPT;


class Accesorio extends conexion
{
    public $id_usuario;
    public $nombreAccesorio;
    public $cantidadExistencia;
    public $precio;
    public $descripcion;


    public function __construct()
    {
        parent::__construct();
    }

    function crear()
    {
        $pre = mysqli_prepare($this->con, "INSERT INTO accesorios(nombre_accesorio,Cantidad_existente,precio,descripcion) VALUES(?,?,?,?)");
        $pre->bind_param("sids",$this->nombreAccesorio,$this->cantidadExistencia,$this->precio,$this->descripcion);
        $pre->execute();
    }
    static function all(){
        $me= new conexion();
        $pre = mysqli_prepare($me->con,"SELECT * FROM accesorios");
        $pre->execute();
        $res = $pre->get_result();
        while ($y=mysqli_fetch_assoc($res)){
            $t[]=$y;
        }
        return $t;
    }

}