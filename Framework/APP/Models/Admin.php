<?php


namespace UPT;


class Admin extends conexion
{
    public $id_usuario;
    public $nombre;
    public $apellidoPaterno;
    public $apellidoMaterno;
    public $correo;
    public $puesto;
    public $password;

    public function __construct()
    {
        parent::__construct();
    }

    function crear()
    {
        $pre = mysqli_prepare($this->con, "INSERT INTO Administrador(Nombre,Apellido_Paterno,Apellido_Materno,Puesto,Correo,Password) VALUES(?,?,?,?,?,?)");
        $pre->bind_param("ssssss",$this->nombre,$this->apellidoPaterno,$this->apellidoMaterno,$this->puesto,$this->correo,$this->password);
        $pre->execute();
    }
    static function verificarAdmin($correo,$password){
        $conexion = new Conexion();
        $pre = mysqli_prepare($conexion->con,"SELECT * FROM Administrador WHERE correo = ? AND password = ?");
        $pre->bind_param("ss",$correo,$password);
        $pre->execute();
        $resultado = $pre->get_result();
        return $resultado->fetch_object();
    }


}