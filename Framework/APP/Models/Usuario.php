<?php


namespace UPT;


class Usuario extends conexion
{
    public $id_usuario;
    public $nombre;
    public $apellidoPaterno;
    public $apellidoMaterno;
    public $correo;
    public $fechaNaci;
    public $password;

    public function __construct()
    {
        parent::__construct();
    }

    function crear()
    {
        $pre = mysqli_prepare($this->con, "INSERT INTO registros(nombre,apellido_paterno,apellido_materno,correo,fecha_naci,password) VALUES(?,?,?,?,?,?)");
        $pre->bind_param("ssssss",$this->nombre,$this->apellidoPaterno,$this->apellidoMaterno,$this->correo,$this->fechaNaci,$this->password);
        $pre->execute();
    }
    static function verificarUsuario($correo,$password){
        $conexion = new Conexion();
        $pre = mysqli_prepare($conexion->con,"SELECT * FROM registros WHERE correo = ? AND password = ?");
        $pre->bind_param("ss",$correo,$password);
        $pre->execute();
        $resultado = $pre->get_result();
        return $resultado->fetch_object();
    }

    static function all(){
        $me=new conexion();
        $pre=mysqli_prepare($me->con,"SELECT * FROM registros ");
        $pre->execute();
        $res = $pre->get_result();
        while ($y=mysqli_fetch_assoc($res)){
            $t[]=$y;
        }
        return $t;
    }
    

}

